package anagramer

import (
	"context"
	"encoding/json"
	"io"
	"net/http"
	"strings"

	"github.com/pkg/errors"
	"github.com/rs/zerolog"
)

type AnagramService interface {
	Fetcher
	BulkInserter
}

type BulkInserter interface {
	BulkInsert(ctx context.Context, words ...string) (BulkInsertResult, error)
}

type Fetcher interface {
	FetchAll(ctx context.Context, words string) ([]string, error)
}

type HTTPAnagramerAPI struct {
	s            AnagramService
	minBatchSize int
}

func NewHTTPAnagramerAPI(s AnagramService, minBatchSize int) *HTTPAnagramerAPI {
	return &HTTPAnagramerAPI{
		s:            s,
		minBatchSize: minBatchSize,
	}
}

func (api *HTTPAnagramerAPI) GetAnagrams(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	logger := zerolog.Ctx(ctx)

	target := r.URL.Query().Get("word")
	if target == "" {
		http.Error(w, "word is not specified", http.StatusBadRequest)
		return
	}

	anagrams, err := api.s.FetchAll(ctx, target)
	if err != nil {
		switch err {
		case ErrAnagramsNotFound:
			// by task need to return null?
			//w.WriteHeader(http.StatusNoContent)
			//return
		default:
			http.Error(w, err.Error(), anagramServiceErr2Code(err))
			return
		}
	}

	w.Header().Set("Content-Type", "application/json")
	if err := json.NewEncoder(w).Encode(anagrams); err != nil {
		logger.Error().Err(err).Msg("encode anagrams to JSON")
		return
	}

	return
}

func (api *HTTPAnagramerAPI) AddWords(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	logger := zerolog.Ctx(ctx)

	processor := NewJSONWordStreamProcessor(api.s)
	response, err := processor.Process(ctx, r.Body, api.minBatchSize)
	if err != nil {
		http.Error(w, "process JSON words stream", http.StatusBadRequest)
		return
	}

	if response.TotalWords == 0 {
		http.Error(w, "no words", http.StatusBadRequest)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	if err := json.NewEncoder(w).Encode(response); err != nil {
		logger.Error().Err(err).Msg("encode anagrams to JSON")
		return
	}

	return
}

type JSONStreamInserter struct {
	s BulkInserter
}

func NewJSONWordStreamProcessor(s BulkInserter) JSONStreamInserter {
	return JSONStreamInserter{
		s: s,
	}
}

type JSONWordStreamProcessResult struct {
	TotalWords int    `json:"total_words"`
	Insterted  int    `json:"insterted"`
	Duplicates int    `json:"duplicates"`
	Error      string `json:"error,omitempty"`
}

func (wsp *JSONStreamInserter) Process(ctx context.Context, r io.Reader, minBatchSize int) (JSONWordStreamProcessResult, error) {
	logger := zerolog.Ctx(ctx)

	decoder := json.NewDecoder(r)

	var wordBuffer []string
	var word string
	var batch []string

	ready := make(chan struct{}, 1)
	defer close(ready)

	inProcess := false

	response := JSONWordStreamProcessResult{}

	_, err := decoder.Token()
	if err != nil {
		return response, errors.Wrap(err, "read opening JSON token")
	}

	for {
		hasNextWord := decoder.More()

		if hasNextWord {
			if err := decoder.Decode(&word); err != nil {
				hasNextWord = false
				response.Error = errors.Wrap(err, "decode message").Error()
			} else {
				word = strings.TrimSpace(word)

				if word == "" {
					continue
				}

				wordBuffer = append(wordBuffer, word)
			}
		}

		readyBatch := len(wordBuffer) > minBatchSize
		if readyBatch || !hasNextWord {
			select {
			case _, ok := <-ready:
				if !ok {
					return response, nil
				}
			default:
				if hasNextWord {
					continue
				}

				if inProcess {
					inProcess = false

					_, ok := <-ready
					if !ok {
						return response, nil
					}
				}
			}

			if len(wordBuffer) == 0 {
				break
			}

			batch = append(batch[:0], wordBuffer...)
			wordBuffer = wordBuffer[:0]
			inProcess = true

			go func(batch []string) {
				logger.Debug().Msgf("insert %d words", len(batch))
				result, err := wsp.s.BulkInsert(ctx, batch...)
				if err != nil {
					response.Error = errors.Wrap(err, "bulk insert").Error()
					logger.Error().Err(err).Msg("bulk insert")
				}

				response.Insterted += result.Insterted
				response.Duplicates += result.Duplicates
				response.TotalWords += result.TotalWords

				ready <- struct{}{}
			}(batch)

			if !hasNextWord {
				<-ready
				break
			}
		}
	}

	return response, nil
}

func anagramServiceErr2Code(err error) int {
	switch err {
	case ErrAnagramsNotFound:
		return http.StatusNotFound
	default:
		return http.StatusInternalServerError
	}
}
