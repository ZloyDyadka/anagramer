package anagramer_test

import (
	"context"
	"testing"

	"github.com/pkg/errors"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/ZloyDyadka/anagramer"
)

func TestHashAnagram(t *testing.T) {
	type args struct {
		s string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{"basic", args{"acbd界世世f"}, "abcdf世世界"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			key := anagramer.BuildAnagramKey(tt.args.s)
			assert.Equal(t, tt.want, key)
		})
	}
}

func TestStorage_FetchAll(t *testing.T) {
	type args struct {
		ctx    context.Context
		target string
	}
	tests := []struct {
		name    string
		storage *anagramer.Storage
		args    args
		want    []string
		wantErr error
	}{
		{
			name:    "basic",
			storage: createStorageWithWords(t, []string{"foobar", "barfoo", "boofar", "test"}),
			args:    args{context.TODO(), "foobar"},
			want:    []string{"foobar", "barfoo", "boofar"},
		},
		{
			name:    "UTF8",
			storage: createStorageWithWords(t, []string{"世界世", "界世世", "界界界", "ponel"}),
			args:    args{context.TODO(), "世界世"},
			want:    []string{"世界世", "界世世"},
		},
		{
			name:    "not found error",
			storage: createStorageWithWords(t, []string{"foobar", "barfoo", "boofar"}),
			args:    args{context.TODO(), "neponel"},
			wantErr: anagramer.ErrAnagramsNotFound,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := tt.storage.FetchAll(tt.args.ctx, tt.args.target)
			assert.Equal(t, tt.wantErr, err)

			assert.Subset(t, got, tt.want)
		})
	}
}

func TestStorage_BulkInsert(t *testing.T) {
	type args struct {
		ctx   context.Context
		words []string
	}
	tests := []struct {
		name    string
		storage *anagramer.Storage
		args    args
		want    anagramer.BulkInsertResult
		wantErr error
	}{
		{
			name:    "duplicate word",
			storage: createStorageWithWords(t, []string{"foobar"}),
			args:    args{context.TODO(), []string{"foobar"}},
			wantErr: nil,
			want: anagramer.BulkInsertResult{
				Duplicates: 1,
				Insterted:  0,
				TotalWords: 1,
			},
		},
		{
			name:    "case insensitive index",
			storage: createIndexCaseInsensitiveStorageWithWords(t, []string{"foobar", "FOObar"}),
			args:    args{context.TODO(), []string{"foobar", "FOObar"}},
			wantErr: nil,
			want: anagramer.BulkInsertResult{
				Duplicates: 0,
				Insterted:  2,
				TotalWords: 2,
			},
		},
		{
			name:    "case sensitive index",
			storage: createIndexCaseInsensitiveStorageWithWords(t, []string{"foobar", "FOObar"}),
			args:    args{context.TODO(), []string{"foobar"}},
			wantErr: nil,
			want: anagramer.BulkInsertResult{
				Duplicates: 1,
				Insterted:  1,
				TotalWords: 2,
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			_, err := tt.storage.BulkInsert(tt.args.ctx, tt.args.words...)
			assert.Equal(t, tt.wantErr, errors.Cause(err))
		})
	}
}

func createStorageWithWords(t *testing.T, words []string) *anagramer.Storage {
	s := anagramer.NewStorage()
	_, err := s.BulkInsert(context.TODO(), words...)
	require.NoError(t, err)

	return s
}

func createIndexCaseInsensitiveStorageWithWords(t *testing.T, words []string) *anagramer.Storage {
	s := anagramer.NewStorage(anagramer.StorageCaseInsensitiveIndex())
	_, err := s.BulkInsert(context.TODO(), words...)
	require.NoError(t, err)

	return s
}
