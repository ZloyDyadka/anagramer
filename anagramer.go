package anagramer

import (
	"context"
	"sort"
	"strings"
	"sync"

	"github.com/pkg/errors"
	"github.com/rs/zerolog"
)

var (
	ErrAnagramsNotFound = errors.New("anagrams not found")
	ErrWordIsExists     = errors.New("word is exists")
)

type anagramsData struct {
	values  []string
	indexes map[string]struct{}
}

func (ad *anagramsData) addWord(index string, val string) {
	ad.values = append(ad.values, val)
	ad.indexes[index] = struct{}{}
}

type Storage struct {
	anagrams             map[string]*anagramsData
	indexCaseInsensitive bool
	l                    sync.RWMutex
}

type StorageOpts func(s *Storage)

func StorageCaseInsensitiveIndex() func(s *Storage) {
	return func(s *Storage) {
		s.indexCaseInsensitive = true
	}
}

func NewStorage(opts ...StorageOpts) *Storage {
	s := &Storage{
		anagrams: make(map[string]*anagramsData),
	}

	for _, opt := range opts {
		opt(s)
	}

	return s
}

func (s *Storage) FetchAll(ctx context.Context, target string) ([]string, error) {
	key := BuildAnagramKey(target)

	s.l.RLock()
	defer s.l.RUnlock()

	anagrams, isExists := s.anagrams[key]
	if !isExists {
		return nil, ErrAnagramsNotFound
	}

	return anagrams.values, nil
}

type BulkInsertResult struct {
	Duplicates int
	Insterted  int
	TotalWords int
}

func (s *Storage) BulkInsert(ctx context.Context, words ...string) (BulkInsertResult, error) {
	logger := zerolog.Ctx(ctx)

	var lastErr error
	result := BulkInsertResult{}

	s.l.Lock()
	defer s.l.Unlock()

	for _, w := range words {
		if err := s.insert(ctx, w); err != nil {
			logger := logger.With().Str("word", w).Logger()

			errCause := errors.Cause(err)
			switch errCause {
			case ErrWordIsExists:
				result.Duplicates++
				logger.Debug().Msg("word is exists")
			default:
				logger.Err(err).Msg("insert word")
				lastErr = err
			}

			result.TotalWords++
			continue
		}

		result.Insterted++
	}

	if result.Insterted == 0 && lastErr != nil {
		return result, errors.Wrap(lastErr, "no words inserted")
	}

	return result, nil
}

func (s *Storage) insert(ctx context.Context, word string) error {
	hash := BuildAnagramKey(word)
	index := word
	if !s.indexCaseInsensitive {
		index = strings.ToLower(word)
	}

	as, isCreated := s.anagrams[hash]
	if !isCreated {
		as = &anagramsData{
			values:  make([]string, 0, 100),
			indexes: make(map[string]struct{}, 100),
		}

		s.anagrams[hash] = as

		as.addWord(index, word)

		return nil
	}

	_, isDup := as.indexes[index]
	if isDup {
		return ErrWordIsExists
	}

	as.addWord(index, word)

	return nil
}

func BuildAnagramKey(s string) string {
	key := []rune(strings.ToLower(s))
	sort.Sort(sortRunes(key))

	return string(key)
}

type sortRunes []rune

func (s sortRunes) Less(i, j int) bool {
	return s[i] < s[j]
}

func (s sortRunes) Swap(i, j int) {
	s[i], s[j] = s[j], s[i]
}

func (s sortRunes) Len() int {
	return len(s)
}
