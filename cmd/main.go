package main

import (
	"net/http"
	"os"
	"time"

	"github.com/pkg/errors"
	"github.com/rs/zerolog/hlog"
	"github.com/rs/zerolog/log"
	"gitlab.com/ZloyDyadka/anagramer"
)

const (
	DefaultBindAddr     = ":8080"
	DefaultMinBatchSize = 10
)

var (
	defaultLogger = log.Logger
)

func main() {
	if err := Run(); err != nil {
		defaultLogger.Fatal().Err(err).Msg("run application")
	}
}

func Run() error {
	logger := defaultLogger.With().Logger()

	bindAddr := DefaultBindAddr
	if addr := os.Getenv("BIND_ADDR"); addr != "" {
		bindAddr = addr
	}

	storage := anagramer.NewStorage(anagramer.StorageCaseInsensitiveIndex())
	httpAPI := anagramer.NewHTTPAnagramerAPI(storage, DefaultMinBatchSize)

	mux := http.NewServeMux()
	mux.HandleFunc("/get", httpAPI.GetAnagrams)
	mux.HandleFunc("/load", httpAPI.AddWords)

	handler := handler{}
	handler.Append(hlog.NewHandler(logger))
	handler.Append(hlog.URLHandler("http_request_url"))
	handler.Append(hlog.RequestIDHandler("request_id", "X-Request-ID"))
	handler.Append(hlog.AccessHandler(accessLog))

	if err := http.ListenAndServe(bindAddr, handler.Then(mux)); err != nil {
		return errors.Wrap(err, "listen and serve HTTP API")
	}

	return nil
}

type middleware func(http.Handler) http.Handler
type handler struct {
	m []middleware
}

func (a *handler) Append(m middleware) {
	a.m = append(a.m, m)
}

func (a *handler) Then(h http.Handler) http.Handler {
	for i := range a.m {
		h = a.m[len(a.m)-1-i](h)
	}

	return h
}

func accessLog(r *http.Request, status int, size int, duration time.Duration) {
	logger := hlog.FromRequest(r)

	logger.Info().
		Int("http_status_code", status).
		Dur("http_response_duration", duration).
		Int("http_body_bytes_sent", size).
		Msg("")
}
